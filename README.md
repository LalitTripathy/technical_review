># <b>TECHNICAL PAPER</b>
<br>

# FIREWALL SYSTEM

###  **<div style="text-align: Center">In general, Firewalls are tools that can be used to enhance the security of computers connected to a network, such as  a LAN or the internet.firewalls acts as gaurds in a Computer.**</div>
<br></br>

<p align="center"><img src="https://www.tortechnologies.com/wp-content/uploads/2020/11/firewall-secure-network-business-768x576.png"/>
</p>





# Introduction 

Security is the most essential perspective in a system. There are a ton of ideas for system security.Firewall is a standout amongst the most imperative ideas identified with the system security. A Firewall can be implemented as software or it can be a hardware **that basically blocks unauthorized communication going out or coming to a network.**
Firewalls are much of the time used to prevent unapproved internet users to get access to private systems
connected with the Internet. All information that is coming and going outside the networks need to pass
through the firewall, which analyze each and every packet and block those that don't meet the certain
security policies. 

Generally, firewalls are configured to secure against unauthenticated intuitive logins
from third party. This helps prevent hackers to log into computer system on your network. Additionally
firewalls that are complex may block traffic from the outside to within, yet allow users within to communicate a little more freely with the outside.

<p align="center">
  <img src="https://www.layerstack.com/img/docs/resources/winfirewall16.jpg"/>
</p>


<br>

# **Key Features of a Firewall**


* Control and arrange traffic

* Authentication access
* Protect organization assets
* Maintain and provide details regarding events
* Work as a mediator 
* Protect network resources from the harmful actions while accessing internet.
* Assure protected and secure access to your internal assets to outside users
* May increase performance of network system.
* **Firewall can be a Software installed in the Computer and A firewall present as a hardware in switch and routers which is placed between your network and untrusted internet.** 

<br>

## **What Firewall Software Does ?**
<br>

Firewalls act as a filter between a home network and the Internet. User can configure in its own way what
information he wants to get away and what he wants to get inside.There are various different approaches or techniques firewalls use to filter traffic, and some of them are
used together. These techniques function at various levels of a network, which decides how proper the
filtering options can be at each level.
<br> 

## **What does a firewall not do ?**
<br>

Although a firewall manages traffic efficiently but still it does not provide full security and can make you
fully safe on the internet.It is considered to be one of the first lines of defense, but all alone it will not
secure you totally

A portion of the things a firewall does not secure you against are mentioned as follow: 

1. Viruses, worms and spam messages

2. Wireless Network that is not configured well.
3. Installation of malware software (secures you from spyware activities, but these activities may
still be available in your PC)

<br> 

## **Types of Firewall Technique**
<br>

1. Packet-Filtering firewall
2. Stateful inspection firewall
3. Circuit - level gateway
4. Proxy or generation 


<br>


## **1. Packet-Filtering firewall**
<br>
<p align="center">
  <img src="https://1.bp.blogspot.com/-sOi7nDJGpJk/VrIzxUjV0GI/AAAAAAAAAXM/Ml-UxdCXebc/s1600/packet-filtering-firewalls.gif"/>
</p>


* This technique is based on most fundamental and oldest type of firewall model.

* Packet-filtering
firewalls essentially make a checkpoint at a traffic switch or router. The firewall directly check the
information packets passing through router or switch 
* for example, the source and destination IP
address, packet number, port number, and other data without opening up the packet to investigate its
information. 
* It works on network layer of network model. This technique applies a lot of principles
(in view of content of IP and transport header fields) on every packet and dependent on the result,
chooses to either transfer or dispose of the packet. 



**Advantages of packet firewall:**
* The beneficial thing about these firewalls is that they don’t consume much resource.
This implies they don't have large effect on system performance and are moderately simple.
* Low cost as few resources are utilized.
* These firewalls are also additionally easy to bypass than other firewalls with more powerful examination features.

**Disadvantages of packet firewall:**
* Subject to IP spoofing
* Does not have any knowledge about packet information hence provides low security.
Examines IP and TCP headers as it operates only on network layer.

<br>

## **2. Stateful firewall**
<br>

* This technique is also called ‘Dynamic Packet Filtering’. Stateful
firewall basically keeps track of the status of active links and uses this information to decide
which packet should be allowed through it. 

* In this approach, firewall keeps a record of dynamic
TCP and UDP sessions information in tabular form including session's sender and recipient IP,
port numbers, and also TCP sequence number. Records are made for only those UDP or TCP
connections that fulfill characterized security criteria’s; packets related with these sessions are
allowed to go through the firewall. 

**Advantages & Disadvantage of packet StateFul Inspection Firewalls:**

<p align="center">
  <img src="https://images.slideplayer.com/24/7474880/slides/slide_29.jpg"width="700" height="600"/>
</p>

<br>

## **3. Circuit-level gateway firewalls**
<br>

Circuit level firewalls are operated at the Session layer of the
network model and they monitor TCP (three way handshake) connection to verify that a
requested connection is authenticated or not.

<p align="center">
  <img src="https://images.slideplayer.com/24/7474880/slides/slide_28.jpg"/>
</p>

<br>

## **4. Application Gateway**
<br></br>

Application firewalls review network packets to check whether data is
valid (at the application layer) before allow making a connection. These firewalls also validate other security information like user passwords and service
requests. Application or proxy services are used for specific reason in order to control traffic such
as FTP or HTTP.<b>It is also called Proxy server.</b>

Application proxy gateway firewalls have more advantages than packet filter firewalls and stateful inspection firewalls. First firewalls have more comprehensive logging capabilities because they are able to examine the entire network addresses and ports.


**Advantages of packet StateFul Inspection Firewalls:**

* recognize the protocols such as HTTP and URL
* has event and logging mechanism
* can do processing and manipulating on packet data
* do not allow a direct connection between endpoints.
more control over traffic passing through the firewall

**Disadvantage of packet StateFul Inspection Firewalls:**

* Slower than packet filtering and stateful packet inspection
* Some protocols such as Smtp or HTTP require own gateway proxy 
* Require extra client configuration.
* High costs.
<p align="center">
  <img src="12345.png"/>
</p>


# Conclusion

<p align="justify">As the Internet becomes more a part of business, firewalls are becoming an important element of an overall network security policy. It plays an important role in computer system security against viruses, spyware, Trojans and other malwares attacks from outside of network. A good firewall provides full security to our network and system without making any influence on the speed of computer system and network access.</p>

In order to provide security, one should always keep some points in mind: 
* One should never install any software from suspected sources. 
* Always download from the respected sites available on internet. Secure your firewall firstly and then use it to monitor all information that we want to transfer over the internet. 
* On each PC a firewall software must be installed else it will to become infected and very fast it will impact all PCs connected to that network.

# References

1. Dr. Ajit singh, Madhu Pahal, Neeraj Goyat,” A Review Paper On Firewall”, School Of Engineering And Sciences, Bhagat
Phool Singh Mahila Vishwavidyalaya Sonipat (Haryana),September (2013).

2. FIREWALLS, available at: http://mercury.webster.edu/aleshunas/COSC%205130/Chapter-22.pdf

 3. Firewalls, Version 2 CSE IIT , Kharagpur, nptel available at: https://nptel.ac.in/courses/106105080/pdf/M8L3.pdf
